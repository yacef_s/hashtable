class Stack:
    def __init__(self):
        self.stack = []

    def is_empty(self):
        if not self.stack:
            return True
        else:
            return False

    def push(self, e):
        self.stack.append(e)

    def pop(self):
        i = len(self.stack) - 1
        return self.stack.pop(i)
