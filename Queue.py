class Queue:
    def __init__(self):
        self.queue = []

    def is_empty(self):
        if not self.queue:
            return True
        else:
            return False

    def enqueue(self, e):
        self.queue.append(e)

    def dequeue(self):
        return self.queue.pop(0)
